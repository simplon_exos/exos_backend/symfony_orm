<?php

namespace App\DataFixtures;

use App\Entity\Bike;
use App\Entity\Rental;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Les fixtures permettent de créer un jeu de données de développement
 * qui pourra être partagé par toute l'équipe et relancer en une
 * commande (bin/console doctrine:fixtures:load)
 * A l'intérieur, on fera en sorte de faire des boucles pour créer des
 * instances des entités dont on veut des exemples et les faire persister
 * Si on veut des données qui font un peu plus "vraie", on peut utiliser
 * la librairie Faker (https://github.com/fzaninotto/Faker) qui permet
 * de générer des nom/email/date/image/description etc.
 */
class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i < 5; $i++) {

            $rental = new Rental();
            $rental->setCity('City ' . $i)
                ->setName('Name ' . $i)
                ->setPro($i % 2 == 0);
            $manager->persist($rental);
            for ($y = 1; $y < 5; $y++) {
                $bike = new Bike();
                $bike->setColor('#00' . $y)
                    ->setElectric($y % 2 == 0)
                    ->setModel('Model ' . $i . $y)
                    ->setGears($y)
                    ->setRental($rental);
                $manager->persist($bike);
            }
        }

        $manager->flush();
    }
}
